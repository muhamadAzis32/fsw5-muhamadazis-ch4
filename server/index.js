/**
 * Impor HTTP Standar Library dari Node.js
 * Hal inilah yang nantinya akan kita gunakan untuk membuat
 * HTTP Server
 * */
const path = require('path');
const express = require('express');
const app = express();
const port = 5000;

//akses path
app.use(express.static(__dirname + '/../public/'));

//Routing
app.get('/', function (req, res) {
  res.sendFile(path.join(__dirname + '/../public/index.html'));
});

app.get('/cars', function (req, res) {
  res.sendFile(path.join(__dirname + '/../public/cars.html'));
});

// Jalankan server
app.listen(port, () => {
  console.log(`Server bejalan silahkan akses : http://localhost:${port}`)
})

